-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for simoring
CREATE DATABASE IF NOT EXISTS `simoring` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simoring`;

-- Dumping structure for table simoring.t_kegiatan
CREATE TABLE IF NOT EXISTS `t_kegiatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kegiatan` varchar(50) DEFAULT '0',
  `deskripsi` text,
  `bobot` int(11) DEFAULT '0',
  `status_kegiatan` int(11) DEFAULT '0',
  `tgl_mulai` timestamp NULL DEFAULT NULL,
  `tgl_rencana_selesai` timestamp NULL DEFAULT NULL,
  `tgl_selesai` timestamp NULL DEFAULT NULL,
  `proyek_id` int(11) DEFAULT NULL,
  `jenis_kegiatan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table simoring.t_kegiatan: ~1 rows (approximately)
/*!40000 ALTER TABLE `t_kegiatan` DISABLE KEYS */;
INSERT INTO `t_kegiatan` (`id`, `nama_kegiatan`, `deskripsi`, `bobot`, `status_kegiatan`, `tgl_mulai`, `tgl_rencana_selesai`, `tgl_selesai`, `proyek_id`, `jenis_kegiatan`) VALUES
	(1, 'as', 'Test', 10, 2, '2022-07-14 00:00:00', '2022-07-14 00:00:00', '2022-07-14 14:31:46', 6, 'Persiapan');
/*!40000 ALTER TABLE `t_kegiatan` ENABLE KEYS */;

-- Dumping structure for table simoring.t_proyek
CREATE TABLE IF NOT EXISTS `t_proyek` (
  `id_proyek` int(11) NOT NULL AUTO_INCREMENT,
  `nama_proyek` varchar(50) DEFAULT '0',
  `client_id` int(11) DEFAULT '0',
  `jenis_proyek` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `tgl_pengajuan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_proyek`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table simoring.t_proyek: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_proyek` DISABLE KEYS */;
INSERT INTO `t_proyek` (`id_proyek`, `nama_proyek`, `client_id`, `jenis_proyek`, `status`, `tgl_pengajuan`) VALUES
	(6, 'Simoring', 14, 'Web', 1, '2022-07-14');
/*!40000 ALTER TABLE `t_proyek` ENABLE KEYS */;

-- Dumping structure for table simoring.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `alamat` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Dumping data for table simoring.user: ~5 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`, `role_id`, `is_active`, `date_created`, `nama`, `no_hp`, `alamat`) VALUES
	(1, 'root', '$2y$10$uWLALzfXTg8bkqwzP7I5n.JiqK4hGNi0ylw89vZ0s1pRlXY0AxLlC', 1, 1, 1648126951, 'User Root', '879879', 'SUbang'),
	(13, 'prayogaep', '$2y$10$KVsmazqka.dGSXid3eS.0e1blH0J1EMQRFLqTYsOXZHyjL3UGMVfi', 2, 1, 1656228103, 'Prayoga Erlangga Putra', NULL, NULL),
	(14, 'rizal', '$2y$10$S/2YAJARTX5PYgCOe3pXkOtawadbWHG84Uc5vaGxeimc5z9oHGjLW', 4, 1, 1656228119, 'Rizal', NULL, NULL),
	(15, 'muksin', '$2y$10$iF1W.GhTjR800sYawwblZOaLrw33OIE1AEIhbNgkDha0QMQXp8O.u', 3, 1, 1656228141, 'muksinalatas', NULL, NULL),
	(16, 'iman', '$2y$10$v7eziShkEVBZBKVH2GRNi.aYNwbbKFGmujKbEvvi4ECECwaFQrujK', 4, 1, 1656228379, 'iman', NULL, NULL),
	(17, 'sandi', '$2y$10$ZqF.KPJaMR1RWiNUSI/l8Og4itVUOf3FINHsqsCXNrlH7y8lGe/dy', 1, 1, 1657805424, 'sandi', '908098080', 'SUbang');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table simoring.user_akses_menu
CREATE TABLE IF NOT EXISTS `user_akses_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table simoring.user_akses_menu: ~7 rows (approximately)
/*!40000 ALTER TABLE `user_akses_menu` DISABLE KEYS */;
INSERT INTO `user_akses_menu` (`id`, `role_id`, `menu_id`) VALUES
	(1, 1, 1),
	(2, 1, 2),
	(3, 2, 2),
	(5, 1, 3),
	(6, 1, 4),
	(7, 4, 4),
	(8, 3, 3);
/*!40000 ALTER TABLE `user_akses_menu` ENABLE KEYS */;

-- Dumping structure for table simoring.user_menu
CREATE TABLE IF NOT EXISTS `user_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table simoring.user_menu: ~4 rows (approximately)
/*!40000 ALTER TABLE `user_menu` DISABLE KEYS */;
INSERT INTO `user_menu` (`id`, `menu`) VALUES
	(1, 'Admin'),
	(2, 'Kabid'),
	(3, 'Programmer'),
	(4, 'Client');
/*!40000 ALTER TABLE `user_menu` ENABLE KEYS */;

-- Dumping structure for table simoring.user_role
CREATE TABLE IF NOT EXISTS `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table simoring.user_role: ~4 rows (approximately)
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` (`id`, `role`) VALUES
	(1, 'Root'),
	(2, 'Kabid'),
	(3, 'Programmer\r\n'),
	(4, 'Client');
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;

-- Dumping structure for table simoring.user_sub_menu
CREATE TABLE IF NOT EXISTS `user_sub_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- Dumping data for table simoring.user_sub_menu: ~8 rows (approximately)
/*!40000 ALTER TABLE `user_sub_menu` DISABLE KEYS */;
INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
	(1, 1, 'Dashboard', 'admin', 'fas fa-fw fa-tachometer-alt', 1),
	(4, 1, 'Menu Management', 'menu', 'fa-solid fa-fw fa-grip', 1),
	(5, 1, 'Sub Menu Management', 'menu/submenu', 'fa-solid fa-fw fa-bars', 1),
	(12, 2, 'User Management', 'usermanagement', 'fa-solid fa-fw fa-users', 1),
	(20, 2, 'List Client', '/masterclient', 'fas fa-arrow-left', 0),
	(21, 2, 'Master Proyek', '/masterproyek', 'fas fa-arrow-left', 1),
	(22, 2, 'Timeline', '/masterproyek/timeline', 'fas fa-arrow-left', 1),
	(23, 3, 'Timeline', '/masterproyek/timeline', 'fas fa-arrow-left', 1),
	(24, 4, 'Timeline', '/masterproyek/timeline', 'fas fa-arrow-left', 1);
/*!40000 ALTER TABLE `user_sub_menu` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
