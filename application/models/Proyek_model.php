<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Proyek_model extends CI_Model
{
    public function getProyek()
    {
        $this->db->select('*');
        $this->db->from('t_proyek');
        $this->db->join('user', 'user.id = t_proyek.client_id');
        return $this->db->get()->result_array();
        
    }
    public function getProyekpdf()
    {
        $this->db->select('*');
        $this->db->from('t_proyek');
        $this->db->join('user', 'user.id = t_proyek.client_id');
        return $this->db->get()->result_array();
    }
    public function getProyekCL($id)
    {
        $this->db->select('*');
        $this->db->from('t_proyek');
        $this->db->join('user', 'user.id = t_proyek.client_id');
        $this->db->where('client_id', $id);
        return $this->db->get()->result_array();
        
    }
    public function kegiatanProyek($id)
    {
        $this->db->select('*');
        $this->db->from('t_kegiatan');
        $this->db->join('t_proyek', 't_proyek.id_proyek = t_kegiatan.proyek_id');
        $this->db->where('proyek_id', $id);
        $this->db->order_by('tgl_mulai', 'ASC');
        return $this->db->get()->result_array();
        
    }
    public function persentaseKegiatan($id)
    {
        $query = "SELECT SUM(bobot) as total FROM t_kegiatan WHERE proyek_id = '$id' AND status_kegiatan = 2";
        $result = $this->db->query($query);
        return $result->row();
        
    }
    
    public function getProyekProgress()
    {
        $this->db->select('*');
        $this->db->from('t_proyek');
        $this->db->join('user', 'user.id = t_proyek.client_id');
        $this->db->where_not_in('status', 0);
        return $this->db->get()->result_array();
        
    }
    public function updateData($id, $data)
    {
        $query = "UPDATE t_proyek SET nama_proyek = '$data[nama_proyek]', client_id = $data[client_id], jenis_proyek = '$data[jenis_proyek]', tgl_pengajuan = '$data[tgl_pengajuan]'  WHERE id_proyek = $id";
        return $this->db->query($query);
    }
    public function updateTimeline($data)
    {
        $query = "UPDATE t_proyek SET status = $data[status]  WHERE id_proyek = $data[id_proyek]";
        return $this->db->query($query);
    }
    
    public function cancelData($id)
    {
        $query = "UPDATE t_proyek SET status = 0  WHERE id_proyek = $id";
        return $this->db->query($query);
    }
    public function deleteData($id)
    {
        $query = "DELETE FROM t_proyek WHERE id_proyek = $id";
        return $this->db->query($query);
    }
    public function deleteKegiatan($id)
    {
        $query = "DELETE FROM t_kegiatan WHERE id = $id";
        return $this->db->query($query);
    }
}
