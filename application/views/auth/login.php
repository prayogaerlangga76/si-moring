
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="<?= base_url('assets/')?>auth" class="h1"><b>Simoring</b>LTE</a>
    </div>
    <div class="card-body">
    <?= $this->session->flashdata('message'); ?>
      <p class="login-box-msg">Sign in to start your session</p>

      <form action="<?= base_url('auth')?>" method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Username" value="<?= set_value('username'); ?>" name="username" >
          <?= form_error('username', '<small class="text-danger pl-3">', '</small>'); ?>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" name="password">
          <?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
