  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="<?= base_url('assets/') ?>dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">SIMORING</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">

        <div class="info">
          <a href="#" class="d-block"><?= $user['nama'] ?></a>
        </div>
      </div>


      <!-- Query Menu -->

      <?php
      $role_id = $this->session->userdata('role_id');
      $queryMenu = "SELECT `user_menu` . `id` , `menu` 
                    FROM `user_menu` JOIN `user_akses_menu` ON `user_menu` . `id` = `user_akses_menu` . `menu_id`
                    WHERE `user_akses_menu` . `role_id` = $role_id ORDER BY `user_akses_menu` . `menu_id` ASC";
      $menu = $this->db->query($queryMenu)->result_array();
      ?>
      <!-- LOOPING MENU -->
      <?php foreach ($menu as $m) : ?>
        <div class="text-white">
          <?= $m['menu'] ?>
        </div>

        <!-- Siapkan Sub-menu -->
        <?php
        $menuId = $m['id'];

        $querySubMenu = "SELECT * 
                            FROM `user_sub_menu` JOIN `user_menu` ON `user_sub_menu` . `menu_id` = `user_menu` .`id` WHERE `user_sub_menu` . `menu_id` = $menuId 
                            AND `user_sub_menu`. `is_active` = 1";

        $subMenu = $this->db->query($querySubMenu)->result_array();
        ?>

        <?php foreach ($subMenu as $sm) : ?>
         
            <a class="nav-link" href="<?= base_url($sm['url']); ?>">

              <span><?= $sm['title'];  ?></span></a>
          <?php endforeach; ?>
          <hr class="sidebar-divider">
        <?php endforeach; ?>

            
              <a class="nav-link" href="<?= base_url('auth/logout') ?>">
                <i class="fa-solid fa-fw fa-right-from-bracket"></i>
                <span>Logout</span></a>
            
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

       

            <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>