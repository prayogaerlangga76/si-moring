<div class="modal fade" id="newSubMenuModal" tabindex="-1" role="dialog" aria-labelledby="newSubMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newSubMenuModalLabel">Tambah sub menu baru</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('menu/submenu') ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="title">Nama sub menu</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Nama Submenu">
                    </div>
                    <div class="form-group">
                        <label for="menu_id">Menu</label>
                        <select name="menu_id" id="menu_id" class="form-control">
                            <option value="" disabled selected>Pilih menu</option>
                            <?php foreach ($menu as $m) : ?>
                                <option value="<?= $m['id'] ?>"><?= $m['menu']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="url">Nama url</label>
                        <input type="text" class="form-control" id="url" name="url" placeholder="Nama URL">
                    </div>
                    <div class="form-group">
                        <label for="icon">Nama Icon</label>
                        <input type="text" class="form-control" id="icon" name="icon" placeholder="Nama Icon">
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="1" id="is_active" name="is_active" checked>
                            <label class="form-check-label" for="is_active">
                                Active?
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $i = 0;

foreach ($subMenu as $sm) : $i++; ?>
    <div class="modal fade" id="editSubMenuModal<?= $sm['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editSubMenuModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editSubMenuModalLabel">Tambah sub menu baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?= base_url('menu/updateSubMenu/' . $sm['id']); ?>" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="title">Nama sub menu</label>
                            <input type="text" class="form-control" id="title" name="title" value="<?= $sm['title']; ?>" placeholder="Nama Submenu">
                        </div>
                        <div class="form-group">
                            <label for="menu_id">Menu</label>
                            <select name="menu_id" id="menu_id" class="form-control">
                                <option value="" disabled>Pilih menu</option>
                                <?php foreach ($menu as $m) : ?>
                                    <?php if ($sm['menu_id'] == $m['id']) : ?>
                                        <option value="<?= $m['id'] ?>" selected><?= $m['menu']; ?></option>
                                    <?php else : ?>
                                        <option value="<?= $m['id'] ?>"><?= $m['menu']; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="url">Nama url</label>
                            <input type="text" class="form-control" value="<?= $sm['url']; ?>" id="url" name="url" placeholder="Nama URL">
                        </div>
                        <div class="form-group">
                            <label for="icon">Nama Icon</label>
                            <input type="text" class="form-control" value="<?= $sm['icon']; ?>" id="icon" name="icon" placeholder="Nama Icon">
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <?php if ($sm['is_active'] == 1) : ?>
                                    <input class="form-check-input" type="checkbox" value="1" id="is_active" name="is_active" checked>
                                <?php else : ?>
                                    <input class="form-check-input" type="checkbox" value="1" id="is_active" name="is_active">
                                <?php endif; ?>
                                <label class="form-check-label" for="is_active">
                                    Active?
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Perbaharui</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


<?php endforeach; ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="col-lg-6 mt-2">
                    <?= form_error(
                        'menu',
                        '<div class="alert alert-danger" role="alert">',
                        '</div>'
                    ); ?>

                    <?= $this->session->flashdata('message'); ?>

                    <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newSubMenuModal"> Tambah submenu baru </a>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Menu</th>
                                <th>URL</th>
                                <th>Icon</th>
                                <th>Active</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($subMenu as $sm) : ?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $sm['title'] ?></td>
                                    <td><?= $sm['menu'] ?></td>
                                    <td><?= $sm['url'] ?></td>
                                    <td><?= $sm['icon'] ?></td>
                                    <td><?= $sm['is_active'] ?></td>
                                    <td>
                                        <a href="" class="badge badge-warning" data-toggle="modal" data-target="#editSubMenuModal<?= $sm['id']; ?>">Edit</a>
                                        <a href="<?= base_url('menu/deleteSubMenu/' . $sm['id']); ?>" onclick="return confirm('Yakin mau dihapus?')" class="badge badge-danger">Delete</a>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>