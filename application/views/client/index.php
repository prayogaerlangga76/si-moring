<!-- Modal Update -->
<?php $i = 0;
foreach ($client as $m) : $i++; ?>
    <div class="modal fade" id="editMenuModal<?= $m['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="editMenuModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editMenuModalLabel">Edit Menu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?= base_url('menu/update/' . $m['id']) ?>" method="post">
                    <input type="hidden" name="id" value="<?= $m['id']; ?>">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="menu">Nama menu</label>
                            <input type="text" class="form-control" id="menu" value="<?= $m['menu']; ?>" name="menu" placeholder="Nama menu">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="col-lg-6 mt-2">
                    <?= form_error(
                        'menu',
                        '<div class="alert alert-danger" role="alert">',
                        '</div>'
                    ); ?>

                    <?= $this->session->flashdata('message'); ?>

                    
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Client</th>
                                <th>Username</th>
                                <th>Role</th>
                                <!-- <th>Aksi</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($client as $c) : ?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $c['nama'] ?></td>
                                    <td><?= $c['username'] ?></td>
                                    <td><?= $c['role'] ?></td>
                                    <!-- <td>
                                        <a href="" class="badge badge-warning" data-toggle="modal" data-target="#editMenuModal<?= $c['id'] ?>">Edit</a>
                                        <a href="<?= base_url('masterclient/delete/' . $c['id']); ?>" onclick="return confirm('Anda yakin ingin menghapus data ?')" class="badge badge-danger">Delete</a>
                                    </td> -->
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>