<!DOCTYPE html>
<html lang="en">

<head>
    <title></title>
    <style>
        th,
        td {
            padding: 15px;
        }
        table {
            width: 100%;
        }
        h1 {
            text-align: center;
        }
    </style>
</head>

<body>
    <h1> Laporan Timeline Tanggal <?= date('d-m-Y', strtotime(date('Y-m-d')))?></h1>
    <table border="1">
        <thead>
            <tr>
                <th>NO</th>
                <th>Tanggal Ajuan</th>
                <th>Nama Projek</th>
                <th>Nama Client</th>
                <th>Jenis Projek</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; ?>
            <?php foreach ($proyek as $p) : ?>
                <tr>
                    <td><?= $i; ?></td>
                     <td><?= $p['tgl_pengajuan'] ?></td>
                    <td><?= $p['nama_proyek'] ?></td>
                    <td><?= $p['nama'] ?></td>
                    <td><?= $p['jenis_proyek'] ?></td>
                    <?php if ($p['status'] == 0) : ?>
                        <td>
                            <p class="badge badge-dark">Waiting List</p>
                        </td>
                    <?php endif; ?>
                    <?php if ($p['status'] == 1) : ?>
                        <td>
                            <p class="badge badge-primary">Progress</p>
                        </td>
                    <?php endif; ?>
                    <?php if ($p['status'] == 2) : ?>
                        <td>
                            <p class="badge badge-success">Proyek Selesai</p>
                        </td>
                    <?php endif; ?>
                
                </tr>
                <?php $i++; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>

</html>