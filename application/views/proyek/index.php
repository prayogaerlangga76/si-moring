<div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMenuModalLabel">Tambah proyek baru</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('masterproyek') ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nama_proyek">Nama Proyek</label>
                        <input type="text" class="form-control" id="nama_proyek" name="nama_proyek" placeholder="Nama Proyek">
                    </div>
                    <div class="form-group">
                        <label for="jenis_proyek">Jenis Proyek</label>
                        <input type="text" class="form-control" id="jenis_proyek" name="jenis_proyek" placeholder="Jenis Proyek">
                    </div>
                    <div class="form-group">
                        <label for="client_id">Nama Client</label>
                        <select name="client_id" id="client_id" class="form-control">
                            <option value="" selected disabled>Pilih Client</option>
                            <?php foreach ($client as $c) : ?>
                                <option value="<?= $c['id']; ?>"><?= $c['nama']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tgl_pengajuan">Tanggal Pengajuan</label>
                        <input type="date" min="0" class="form-control" id="tgl_pengajuan" name="tgl_pengajuan" placeholder="Nilai Proyek">
                    </div>
                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Update -->
<?php $i = 0;
foreach ($proyek as $p) : $i++; ?>
    <div class="modal fade" id="editMenuModal<?= $p['id_proyek'] ?>" tabindex="-1" role="dialog" aria-labelledby="editMenuModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editMenuModalLabel">Proyek</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?= base_url('masterproyek/update/' . $p['id_proyek']) ?>" method="post">
                    <input type="hidden" name="id_proyek" value="<?= $p['id_proyek']; ?>">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama_proyek">Nama Proyek</label>
                            <input type="text" class="form-control" id="nama_proyek" value="<?= $p['nama_proyek']?>" name="nama_proyek" placeholder="Nama Proyek">
                        </div>
                        <div class="form-group">
                            <label for="jenis_proyek">Jenis Proyek</label>
                            <input type="text" class="form-control" id="jenis_proyek" value="<?= $p['jenis_proyek']?>" name="jenis_proyek" placeholder="Jenis Proyek">
                        </div>

                        <div class="form-group">
                            <label for="client_id">Nama Client</label>
                            <select name="client_id" id="client_id" class="form-control">
                                <option value="" selected disabled>Pilih Client</option>
                                <?php foreach ($client as $c) : ?>
                                    <?php if ($c['id'] == $p['client_id']) : ?>
                                        <option value="<?= $c['id']; ?>" selected><?= $c['nama']; ?></option>
                                    <?php else : ?>
                                        <option value="<?= $c['id']; ?>"><?= $c['nama']; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tgl_pengajuan">Tanggal Pengajuan</label>
                            <input type="date" min="0" class="form-control" id="tgl_pengajuan" value="<?= $p['tgl_pengajuan']?>" name="tgl_pengajuan" placeholder="tgl_pengajuan">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="col-lg-6 mt-2">
                    <?= form_error(
                        'menu',
                        '<div class="alert alert-danger" role="alert">',
                        '</div>'
                    ); ?>

                    <?= $this->session->flashdata('message'); ?>

                    <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newMenuModal"> Tambah Proyek </a>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tanggal Ajuan</th>
                                <th>Nama Proyek</th>
                                <th>Nama Client</th>
                                <th>Jenis Proyek</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($proyek as $p) : ?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $p['tgl_pengajuan'] ?></td>
                                    <td><?= $p['nama_proyek'] ?></td>
                                    <td><?= $p['nama'] ?></td>
                                    <td><?= $p['jenis_proyek'] ?></td>
                                    <?php if ($p['status'] == 0) : ?>
                                        <td>
                                            <p class="badge badge-dark">Waiting List</p>
                                        </td>
                                    <?php endif; ?>
                                    <?php if ($p['status'] == 1) : ?>
                                        <td>
                                            <p class="badge badge-primary">Progress</p>
                                        </td>
                                    <?php endif; ?>
                                    <?php if ($p['status'] == 2) : ?>
                                        <td>
                                            <p class="badge badge-success">Proyek Selesai</p>
                                        </td>
                                    <?php endif; ?>
                                    <?php if ($p['status'] == 0) : ?>
                                        <td>
                                            <a href="" class="badge badge-warning" data-toggle="modal" data-target="#editMenuModal<?= $p['id_proyek'] ?>">Edit</a>
                                            <a href="<?= base_url('masterproyek/delete/' . $p['id_proyek']); ?>" onclick="return confirm('Anda yakin ingin menghapus data ?')" class="badge badge-danger">Delete</a>
                                        </td>
                                    <?php else : ?>
                                        <td></td>
                                    <?php endif; ?>
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>