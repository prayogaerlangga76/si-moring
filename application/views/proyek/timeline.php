<div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMenuModalLabel">Mulai timeline Projek</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('masterproyek/updateTimeline/')  ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="id_proyek">Nama Projek</label>
                        <select name="id_proyek" id="id_proyek" class="form-control">
                            <option value="" selected disabled>Pilih Projek tersedia</option>
                            <?php foreach ($proyekWL as $p) : ?>
                                <option value="<?= $p['id_proyek']; ?>"><?= $p['nama_proyek']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Mulai</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <section class="content">
                    <?= form_error(
                        'menu',
                        '<div class="alert alert-danger" role="alert">',
                        '</div>'
                    ); ?>

                    <?= $this->session->flashdata('message'); ?>
                    <?php if ($user['role_id'] == 2 || $user['role_id'] == 1) : ?>
                        <a href="" class="btn btn-primary mb-3 mt-2 ml-2" data-toggle="modal" data-target="#newMenuModal"> Mulai Timeline Proyek </a>
                    <?php endif; ?>
                    <?php if ($user['role_id'] == 2 || $user['role_id'] == 1) : ?>
                        <a href="<?= base_url('masterproyek/pdf') ?>" class="btn btn-success mb-3 mt-2 mr-5"> Export PDF</a>
                    <?php endif; ?>
                </section>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <table class="table table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tanggal Ajuan</th>
                                <th>Nama Proyek</th>
                                <th>Nama Client</th>
                                <th>Jenis Proyek</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($user['role_id'] == 4) : ?>
                                <?php $i = 1; ?>
                                <?php foreach ($proyekClient as $p) : ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $p['tgl_pengajuan'] ?></td>
                                        <td><?= $p['nama_proyek'] ?></td>
                                        <td><?= $p['nama'] ?></td>
                                        <td><?= $p['jenis_proyek']?></td>
                                        <?php if ($p['status'] == 0) : ?>
                                            <td>
                                                <p class="badge badge-dark">Waiting List</p>
                                            </td>
                                        <?php endif; ?>
                                        <?php if ($p['status'] == 1) : ?>
                                            <td>
                                                <p class="badge badge-primary">Progress</p>
                                            </td>
                                        <?php endif; ?>
                                        <?php if ($p['status'] == 2) : ?>
                                            <td>
                                                <p class="badge badge-success">Proyek Selesai</p>
                                            </td>
                                        <?php endif; ?>
                                        <td>
                                            <a href="<?= base_url('masterproyek/detail/' . $p['id_proyek']) ?>" class="badge badge-info">Detail</a>
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <?php $i = 1; ?>
                                <?php foreach ($proyek as $p) : ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $p['tgl_pengajuan'] ?></td>
                                        <td><?= $p['nama_proyek'] ?></td>
                                        <td><?= $p['nama'] ?></td>
                                        <td><?= $p['jenis_proyek']?></td>
                                        <?php if ($p['status'] == 0) : ?>
                                            <td>
                                                <p class="badge badge-dark">Waiting List</p>
                                            </td>
                                        <?php endif; ?>
                                        <?php if ($p['status'] == 1) : ?>
                                            <td>
                                                <p class="badge badge-primary">Progress</p>
                                            </td>
                                        <?php endif; ?>
                                        <?php if ($p['status'] == 2) : ?>
                                            <td>
                                                <p class="badge badge-success">Proyek Selesai</p>
                                            </td>
                                        <?php endif; ?>
                                        <td>
                                            <a href="<?= base_url('masterproyek/detail/' . $p['id_proyek']) ?>" class="badge badge-info">Detail</a>
                                            <?php if ($p['status'] != 2) : ?>
                                                <a href="<?= base_url('masterproyek/cancelProyek/' . $p['id_proyek']); ?>" onclick="return confirm('Anda yakin ingin membatalkan proyek ?')" class="badge badge-danger">Cancel</a>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                <?php endforeach; ?>
                            <?php endif ?>

                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
