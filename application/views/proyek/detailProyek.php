<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-6">
                    <h1><?= $proyek['nama_proyek']; ?></h1>
                </div>
                <div class="col-6 col-md-12">
                    <?php if ($user['role_id'] == 4) : ?>
                    <?php else : ?>
                        <?php if ($proyek['status'] != 2) : ?>
                        <a href="<?= base_url('masterproyek/selesaiProyek/') . $proyek['id_proyek']; ?>" class="btn btn-success">Selesaikan Project</a>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="col-lg-6 mt-2">
                    <?= form_error(
                        'menu',
                        '<div class="alert alert-danger" role="alert">',
                        '</div>'
                    ); ?>

                    <?= $this->session->flashdata('message'); ?>
                </div>

                <div class="container">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#overview"><i class="fa fa-home"></i> Overview</a></li>

                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Realisasi"><i class="fa fa-vcard"></i> Realisasi</a></li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane show active" id="overview">

                            <!-- batas awal -->
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-12">
                                    <div class="card bg-light">
                                        <div class="card-header">Ring progress bar</div>
                                        <div class="card-body text-center">
                                            <input type="text" class="knob" value="<?= $persentase->total ?>%" data-width="125" data-height="125" data-thickness="0.25" data-fgColor="#cb8fe7" readonly>
                                            <p class="text-muted m-b-0">Progress</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-12">
                                    <div class="card bg-light">

                                        <div class="card-body">
                                            <h6>Formulir Perencanaan Projek</h6>
                                            <?php if ($user['role_id'] == 4) : ?>
                                            <?php else : ?>
                                                <?php if ($proyek['status'] != 2) : ?>
                                                <button data-toggle="modal" data-target="#largeModal" class="btn btn-primary">Tambah Kegiatan</button>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="title" id="largeModalLabel">Tambah Kegiatan</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <!-- batas awal -->
                                                            <div class="table-responsive">
                                                                <form action="<?= base_url('masterproyek/detail/') . $proyek['id_proyek'] ?>" method="post">
                                                                    <!-- <?php echo base_url() ?>proyek/addperencanaan/<?= $this->uri->segment(3); ?> -->
                                                                    <table class="table center-aligned-table" id="dynamic_field">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>No</th>
                                                                                <th>Name Kegiatan</th>
                                                                                <th>Jenis Kegiatan</th>
                                                                                <th>Deskripsi</th>
                                                                                <th>Bobot(%)</th>
                                                                                <th>Tgl Mulai</th>
                                                                                <th>Rencana Selesai</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr id="row1">
                                                                                <input type="hidden" name="persentase" value="<?= $persentase->total ?>">
                                                                                <td>1</td>
                                                                                <td><input type="text" class="form-control" name="nama_kegiatan" placeholder="Nama Kegiatan" required></td>
                                                                                <td>
                                                                                    <select class="custom-select" id="inputGroupSelect04" name="jenis_kegiatan">
                                                                                        <option value="" selected disabled>Pilih Kegiatan</option>
                                                                                        <option value="Persiapan">Persiapan</option>
                                                                                        <option value="Pelaksanaan">Pelaksanaan</option>
                                                                                        <option value="Pelaporan">Pelaporan</option>
                                                                                    </select>
                                                                                </td>
                                                                                <td><input type="text" class="form-control" name="deskripsi" placeholder="Deskripsi"></td>
                                                                                <td>
                                                                                    <div class="form-group"><input type="number" class="form-control prc" name="bobot" placeholder="Bobot (%)" required></div>
                                                                                </td>
                                                                                <td><input type="date" name="tgl_mulai" class="form-control" required></td>
                                                                                <td><input type="date" name="tgl_rencana_selesai" class="form-control" required></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" id="btSubmitmodal" class="btn btn-primary">Tambah</button>
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table table-hover m-b-0">
                                                    <form action="" method="post">
                                                        <!-- <?php echo base_url() ?>proyek/updatekegiatan/<?= $this->uri->segment(3); ?> -->
                                                        <table class="table center-aligned-table" id="dynamic_field">
                                                            <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Nama Kegiatan</th>
                                                                    <th>Jenis Kegiatan</th>
                                                                    <th>Deskripsi</th>
                                                                    <th>Bobot(%)</th>
                                                                    <th>Tanggal Mulai</th>
                                                                    <th>Tanggal Rencana Selesai</th>
                                                                    <th>Tanggal Selesai</th>
                                                                    <th>Aksi</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                $no = 1;
                                                                foreach ($kegiatan as $k) : ?>
                                                                    <tr id="row<?= $k['id'] ?>">
                                                                        <td><?= $no++ ?> <input type="hidden" class="form-control" name="id" value="<?= $k['id'] ?>"></td>
                                                                        <td><?= $k['nama_kegiatan']; ?></td>
                                                                        <!-- <input type="text" class="form-control" name="kegiatan" value="<?= $k['nama_kegiatan'] ?>" placeholder="Nama Kegiatan" required> -->
                                                                        <td><?= $k['jenis_kegiatan']; ?></td>
                                                                        <!-- <select class="custom-select" id="inputGroupSelect04" name="jenis_kegiatan[]">
                                                                                <option value="" selected disabled>Pilih Kegiatan</option>
                                                                                <option value="Persiapan">Persiapan</option>
                                                                                <option value="Pelaksanaan">Pelaksanaan</option>
                                                                                <option value="Pelaporan">Pelaporan</option>
                                                                            </select> -->
                                                                        <td><?= $k['deskripsi']; ?></td>
                                                                        <!-- <input type="text" class="form-control" name="deskripsi" value="<?= $k['deskripsi_kegiatan'] ?>" placeholder="Deskripsi"> -->
                                                                        <td>
                                                                            <?= $k['bobot']; ?>
                                                                            <!-- <div class="form-group"><input type="number" class="form-control prc" name="bobot[]" value="<?= $listkegiatan['bobot_kegiatan'] ?>" placeholder="Bobot (%)" required></div> -->
                                                                        </td>
                                                                        <td><?= date('d-m-Y', strtotime(date($k['tgl_mulai']))) ?></td>
                                                                        <td><?= date('d-m-Y', strtotime(date($k['tgl_rencana_selesai']))) ?></td>
                                                                        <?php if ($k['tgl_selesai'] == NULL) : ?>
                                                                            <td>
                                                                                <p class="badge badge-primary">Belum Selesai</p>
                                                                            </td>
                                                                        <?php else : ?>
                                                                            <td><?= date('d-m-Y', strtotime(date($k['tgl_selesai']))) ?></td>
                                                                        <?php endif; ?>
                                                                        <?php if ($k['status_kegiatan'] != 2) : ?>
                                                                            <?php if ($user['role_id'] == 4) : ?>
                                                                            <?php else : ?>
                                                                                <td>

                                                                                    <a href="<?= base_url() ?>masterproyek/hapusKegiatan/<?= $k['id'] ?>" class="btn btn-danger" onclick="return confirm('are you sure?')"> Hapus</a>
                                                                                </td>
                                                                            <?php endif; ?>
                                                                            <td>
                                                                            </td>
                                                                        <?php else : ?>
                                                                            <td>
                                                                                <p class="badge badge-success">Selesai</p>
                                                                            </td>

                                                                        <?php endif; ?>
                                                                    </tr>
                                                                <?php endforeach; ?>
                                                            </tbody>
                                                        </table>


                                                        <!-- perencanaan -->
                                                        <script>
                                                            $('.form-group').on('input', '.prc', function() {
                                                                var totalSum = 0;
                                                                $('.form-group .prc').each(function() {
                                                                    var inputVal = $(this).val();
                                                                    if ($.isNumeric(inputVal)) {
                                                                        totalSum += parseFloat(inputVal);
                                                                    }
                                                                });
                                                                $('#result').val(totalSum);
                                                                var bt = document.getElementById('btSubmit');
                                                                var bt2 = document.getElementById('btSubmitmodal');
                                                                if (totalSum <= '100') {
                                                                    bt.disabled = false;
                                                                    bt2.disabled = false;
                                                                } else {
                                                                    bt.disabled = true;
                                                                    bt2.disabled = true;
                                                                }
                                                            });
                                                        </script>
                                                    </form>
                                                    <!-- <tbody> -->

                                                    <!-- <?php foreach ($logkegiatan as $listlog) : ?>
                                                                    <tr>
                                                                        <td>
                                                                        <img src="<?= base_url(); ?>assets/fotoprofile/<?= $listlog['foto'] ?>" width="50px" height="50px" class="rounded avatar" alt="">
                                                                        </td>
                                                                        <td>
                                                                            <h6 class="margin-0"><?= $listlog['first_name'] ?> <?= $listlog['last_name'] ?></h6>
                                                                            <span>Merubah status kegiatan <strong><?= $listlog['nama_kegiatan'] ?></strong> dari <?= $listlog['status_kegiatan'] ?> ke <?= $listlog['status_kegiatan_sekarang'] ?> </span>
                                                                        </td>
                                                                        <td class="text-right">
                                                                            <div class="text-success">
                                                                            <span class="badge badge-success"><?= $listlog['aksi'] ?></span>
                                                                            </div>
                                                                            <div class="text-muted"><?= $listlog['waktu_perubahan'] ?></div>
                                                                        </td>
                                                                    </tr>
                                                                    <?php endforeach; ?> -->

                                                    <!-- </tbody> -->
                                                </table>
                                            </div>
                                        </div>
                                    </div>


                                </div>



                                <!-- akhir -->

                            </div>
                            <!-- akhir home -->

                        </div>
                        <div class="tab-pane" id="Perencanaan">
                            <h6>Formulir Perencanaan Projek</h6>
                            <?php if ($user['role_id'] == 4) : ?>
                            <?php else : ?>
                                <button data-toggle="modal" data-target="#largeModal" class="btn btn-primary">Tambah Kegiatan</button>
                            <?php endif; ?>
                            <!-- modals large -->
                            <!-- Large Size -->
                            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="title" id="largeModalLabel">Tambah Kegiatan</h4>
                                        </div>
                                        <div class="modal-body">
                                            <!-- batas awal -->
                                            <div class="table-responsive">
                                                <form action="" method="post">
                                                    <!-- <?php echo base_url() ?>proyek/addperencanaan/<?= $this->uri->segment(3); ?> -->
                                                    <table class="table center-aligned-table" id="dynamic_field">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Name Kegiatan</th>
                                                                <th>Jenis Kegiatan</th>
                                                                <th>Deskripsi</th>
                                                                <th>Bobot(%)</th>
                                                                <th>Tgl Mulai</th>
                                                                <th>Rencana Selesai</th>
                                                                <th>
                                                                    <button type="button" name="add" id="add" class="btn btn-success"><i class="fa fa-plus"></i> ADD</button>
                                                                </th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr id="row1">
                                                                <td>1</td>
                                                                <td><input type="text" class="form-control" name="kegiatan[]" placeholder="Nama Kegiatan" required></td>
                                                                <td><select class="custom-select" id="inputGroupSelect04" name="jenis_kegiatan[]">
                                                                        <!-- <?php foreach ($jenis_kegiatan as $listjeniskegiatan) : ?>
                                                                             <option value="<?= $listjeniskegiatan['idjenis_kegiatan'] ?>"><?= $listjeniskegiatan['namajenis_kegiatan'] ?></option>
                                                                            <?php endforeach; ?> -->
                                                                    </select></td>
                                                                <td><input type="text" class="form-control" name="deskripsi[]" placeholder="Deskripsi"></td>
                                                                <td>
                                                                    <div class="form-group"><input type="number" class="form-control prc" name="bobot[]" placeholder="Bobot (%)" required></div>
                                                                </td>
                                                                <td><input data-provide="datepicker" data-date-format="yyyy-mm-dd" name="tanggalmulai[]" data-date-autoclose="true" class="form-control" required></td>
                                                                <td><input data-provide="datepicker" data-date-format="yyyy-mm-dd" name="tanggalrencanaselesai[]" data-date-autoclose="true" class="form-control" required></td>
                                                                <td><button type="button" name="remove" id="1" class="btn btn-danger btn_remove"><i class="icon-trash"></i></button></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                            </div>
                                            <script>
                                                //     $(document).ready(function(){  
                                                //         var i=1;  
                                                //         $('#add').click(function(){  
                                                //             i++;  
                                                //             $('#dynamic_field').append('<tr id="row'+i+'"><td>'+i+'</td><td><input type="text" name="kegiatan[]" placeholder="Nama Kegiatan" class="form-control" required></td><td><select class="custom-select" id="inputGroupSelect04" name="jenis_kegiatan[]"><?php foreach ($jenis_kegiatan as $listjeniskegiatan) : ?><option value="<?= $listjeniskegiatan['idjenis_kegiatan'] ?>"><?= $listjeniskegiatan['namajenis_kegiatan'] ?></option><?php endforeach; ?></select></td><td><input type="text" name="deskripsi[]" placeholder="Deskripsi" class="form-control" /></td><td><input type="number" name="bobot[]" placeholder="Bobot (%)" class="form-control" required></td><td><input data-provide="datepicker" data-date-format="yyyy-mm-dd" name="tanggalmulai[]" data-date-autoclose="true" class="form-control" required></td><td><input data-provide="datepicker" data-date-format="yyyy-mm-dd" name="tanggalrencanaselesai[]" data-date-autoclose="true" class="form-control" required></td><<td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"><i class="icon-trash"></i></button></td><br>');  
                                                //         });  
                                                //         $(document).on('click', '.btn_remove', function(){  
                                                //             var button_id = $(this).attr("id");   
                                                //             $('#row'+button_id+'').remove();  
                                                //         });
                                                //     });  
                                                // 
                                            </script>


                                            <!-- batas akhir -->
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" id="btSubmitmodal" class="btn btn-primary">SAVE CHANGES</button>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- modals large update perencanaan kegiatan-->
                            <div class="table-responsive">
                                <form action="" method="post">
                                    <!-- <?php echo base_url() ?>proyek/updatekegiatan/<?= $this->uri->segment(3); ?> -->
                                    <table class="table center-aligned-table" id="dynamic_field">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Name Kegiatan</th>
                                                <th>Jenis Kegiatan</th>
                                                <th>Deskripsi</th>
                                                <th>Bobot(%)</th>
                                                <th>Tanggal Mulai</th>
                                                <th>Tanggal Rencana Selesai</th>
                                                <th>Aksi </th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- <?php
                                                    $no = 1;
                                                    foreach ($kegiatan as $listkegiatan) : ?>
                                                                    <tr id="row<?= $listkegiatan['id_kegiatan'] ?>">
                                                                        <td><?= $no++ ?> <input type="hidden" class="form-control" name="id_kegiatan[]" value="<?= $listkegiatan['id_kegiatan'] ?>"></td>
                                                                        <td><input type="text" class="form-control" name="kegiatan[]" value="<?= $listkegiatan['nama_kegiatan'] ?>" placeholder="Nama Kegiatan" required></td>
                                                                        <td><select class="custom-select" id="inputGroupSelect04" name="jenis_kegiatan[]">
                                                                            <?php foreach ($jenis_kegiatan as $listjeniskegiatan) : ?>
                                                                                <?php if ($listkegiatan['jenis_kegiatan'] == $listjeniskegiatan['idjenis_kegiatan']) : ?>
                                                                                    <option value="<?= $listjeniskegiatan['idjenis_kegiatan'] ?>" selected><?= $listjeniskegiatan['namajenis_kegiatan'] ?></option>
                                                                                <?php else : ?>
                                                                                    <option value="<?= $listjeniskegiatan['idjenis_kegiatan'] ?>"><?= $listjeniskegiatan['namajenis_kegiatan'] ?></option>
                                                                                <?php endif; ?>
                                                                            <?php endforeach; ?>
                                                                        </select></td>
                                                                        <td><input type="text" class="form-control" name="deskripsi[]" value="<?= $listkegiatan['deskripsi_kegiatan'] ?>" placeholder="Deskripsi" ></td>
                                                                        <td><div class="form-group"><input type="number" class="form-control prc" name="bobot[]" value="<?= $listkegiatan['bobot_kegiatan'] ?>" placeholder="Bobot (%)" required></div></td>
                                                                        <td><input data-provide="datepicker" data-date-format="yyyy-mm-dd" name="tanggalmulai[]" value="<?= $listkegiatan['tgl_mulai_kegiatan'] ?>" data-date-autoclose="true" class="form-control" required></td>
                                                                        <td><input data-provide="datepicker" data-date-format="yyyy-mm-dd" name="tanggalrencanaselesai[]" value="<?= $listkegiatan['tgl_rencanaselesai'] ?>" data-date-autoclose="true" class="form-control" required></td>
                                                                        <td>
                                                                            <a href="<?= base_url() ?>proyek/hapuskegiatan/<?= $listkegiatan['id_kegiatan'] ?>" class="btn btn-danger" onclick="return confirm('are you sure?')"> <i class="icon-trash"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                    <?php endforeach; ?> -->
                                        </tbody>
                                    </table>

                                    <button class="btn btn-info" id="btSubmit" type="submit"><i class="ace-icon fa fa-check bigger-110"></i>Simpan <output id="result"></output></button>
                                    <!-- perencanaan -->
                                    <script>
                                        $('.form-group').on('input', '.prc', function() {
                                            var totalSum = 0;
                                            $('.form-group .prc').each(function() {
                                                var inputVal = $(this).val();
                                                if ($.isNumeric(inputVal)) {
                                                    totalSum += parseFloat(inputVal);
                                                }
                                            });
                                            $('#result').val(totalSum);
                                            var bt = document.getElementById('btSubmit');
                                            var bt2 = document.getElementById('btSubmitmodal');
                                            if (totalSum <= '100') {
                                                bt.disabled = false;
                                                bt2.disabled = false;
                                            } else {
                                                bt.disabled = true;
                                                bt2.disabled = true;
                                            }
                                        });
                                    </script>
                                </form>
                            </div>
                            <!-- disini -->
                        </div>
                        <div class="tab-pane" id="Realisasi">
                            <h6>Realisasi Kegiatan</h6>
                            <!-- batas awal -->

                            <div class="table-responsive">
                                <form action="<?php echo base_url() ?>proyek/updaterealisasi/<?= $this->uri->segment(3); ?>" method="post">
                                    <table class="table center-aligned-table">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Name Kegiatan</th>
                                                <th>Jenis Kegiatan</th>
                                                <th>Deskripsi</th>
                                                <th>Bobot(%)</th>
                                                <th>Tanggal mulai</th>
                                                <th>Tanggal Rencana Selesai</th>
                                                <th>Tanggal Selesai</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no = 1;
                                            foreach ($kegiatan as $k) : ?>
                                                <tr id="row<?= $k['id'] ?>">
                                                    <td><?= $no++ ?> <input type="hidden" class="form-control" name="id" value="<?= $k['id'] ?>"></td>
                                                    <td><?= $k['nama_kegiatan']; ?></td>
                                                    <!-- <input type="text" class="form-control" name="kegiatan" value="<?= $k['nama_kegiatan'] ?>" placeholder="Nama Kegiatan" required> -->
                                                    <td><?= $k['jenis_kegiatan']; ?></td>
                                                    <!-- <select class="custom-select" id="inputGroupSelect04" name="jenis_kegiatan[]">
                                                                                <option value="" selected disabled>Pilih Kegiatan</option>
                                                                                <option value="Persiapan">Persiapan</option>
                                                                                <option value="Pelaksanaan">Pelaksanaan</option>
                                                                                <option value="Pelaporan">Pelaporan</option>
                                                                            </select> -->
                                                    <td><?= $k['deskripsi']; ?></td>
                                                    <!-- <input type="text" class="form-control" name="deskripsi" value="<?= $k['deskripsi_kegiatan'] ?>" placeholder="Deskripsi"> -->
                                                    <td>
                                                        <?= $k['bobot']; ?>
                                                        <!-- <div class="form-group"><input type="number" class="form-control prc" name="bobot[]" value="<?= $listkegiatan['bobot_kegiatan'] ?>" placeholder="Bobot (%)" required></div> -->
                                                    </td>
                                                    <td><?= date('d-m-Y', strtotime(date($k['tgl_mulai']))) ?></td>
                                                    <td><?= date('d-m-Y', strtotime(date($k['tgl_rencana_selesai']))) ?></td>
                                                    <td><?= date('d-m-Y', strtotime(date($k['tgl_selesai']))) ?></td>
                                                    <?php if ($k['status_kegiatan'] == 2) : ?>
                                                        <td>
                                                            <p class="badge badge-success">Selesai</p>
                                                        </td>
                                                    <?php else : ?>
                                                        <?php if ($user['role_id'] == 4) : ?>
                                                        <?php else : ?>
                                                            <td>
                                                                <a href="<?= base_url() ?>masterproyek/selesaiKegiatan/<?= $k['id'] ?>" class="badge badge-primary" onclick="return confirm('are you sure?')"> Finish</a>
                                                            </td>
                                                        <?php endif; ?>
                                                    <?php endif; ?>

                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>

                                </form>
                            </div>
                            <!-- realisasi -->
                            <script>
                                $('.form-group').on('input', '.rsi', function() {
                                    var totalSum = 0;
                                    $('.form-group .rsi').each(function() {
                                        var inputVal = $(this).val();
                                        if ($.isNumeric(inputVal)) {
                                            totalSum += parseFloat(inputVal);
                                        }
                                    });
                                    $('#resultrealisasi').val(totalSum);
                                    var bt = document.getElementById('btRealisasit');
                                    if (totalSum <= '100') {
                                        bt.disabled = false;
                                    } else {
                                        bt.disabled = true;
                                    }
                                });
                            </script>
                            <!-- batas akhir -->
                        </div>





                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>