<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Masterproyek extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Proyek_model');
        $this->load->model('User_model');
        // $this->load->library ( 'Mcarbon' );
        // $this->load->library('carbon_lib'); 
    }
    public function index()
    {
        $data['title'] = 'Master proyek';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $data['proyek'] = $this->Proyek_model->getProyek();
        $data['client'] = $this->User_model->dataClient();

        if ($data['user']['role_id'] == 2 || $data['user']['role_id'] == 1) {

            $this->form_validation->set_rules('nama_proyek', 'Nama Proyek', 'required');
            $this->form_validation->set_rules('client_id', 'Client', 'required');
            $this->form_validation->set_rules('jenis_proyek', 'Jenis Proyek', 'required');
            $this->form_validation->set_rules('tgl_pengajuan', 'Tanggal Pengajuan', 'required');

            if ($this->form_validation->run() == false) {
                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('proyek/index', $data);
                $this->load->view('templates/footer');
            } else {
                $data = [
                    'nama_proyek' => $this->input->post('nama_proyek'),
                    'client_id' => $this->input->post('client_id'),
                    'jenis_proyek' => $this->input->post('jenis_proyek'),
                    'tgl_pengajuan' => $this->input->post('tgl_pengajuan'),
                    'status' => 0,
                ];
                $this->db->insert('t_proyek', $data);
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    proyek berhasil ditambahkan
                </div>');
                redirect('masterproyek');
            }
        } else {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('blank/404', $data);
            $this->load->view('templates/footer');
        }
    }
    public function timeline()
    {
        $data['title'] = 'Timeline Proyek';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        if ($data['user']['role_id'] == 4) {
            $data['proyekClient'] = $this->Proyek_model->getProyekCL($data['user']['id']);
        } else {
            $data['proyekWL'] = $this->Proyek_model->getProyek();
            $data['proyek'] = $this->Proyek_model->getProyekProgress();
            $data['client'] = $this->User_model->dataClient();
        }


        $this->form_validation->set_rules('nama_proyek', 'Nama Proyek', 'required');
        $this->form_validation->set_rules('client_id', 'Client', 'required');
        $this->form_validation->set_rules('nilai_proyek', 'Nilai Proyek', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('proyek/timeline', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'nama_proyek' => $this->input->post('nama_proyek'),
                'client_id' => $this->input->post('client_id'),
                'nilai_proyek' => $this->input->post('nilai_proyek'),
                'status' => 0,
            ];
            $this->db->insert('t_proyek', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    proyek berhasil ditambahkan
                </div>');
            redirect('masterproyek');
        }
    }
    public function update($id)
    {
        $data = [
            'nama_proyek' => $this->input->post('nama_proyek'),
            'client_id' => $this->input->post('client_id'),
            'jenis_proyek' => $this->input->post('jenis_proyek'),
            'tgl_pengajuan' => $this->input->post('tgl_pengajuan'),
            'status' => 0,
        ];
        $this->Proyek_model->updateData($this->input->post('id_proyek'), $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Proyek berhasil diupdate
          </div>');
        redirect('masterproyek');
    }
    public function selesaiKegiatan($id)
    {
        $kegiatan = $this->db->get_where('t_kegiatan', ['id' => $id])->row_array();
        $tanggal = date('Y-m-d');
        $tanggal_mulai = $this->db->get_where('t_kegiatan', ['id' => $id])->row_array();
        $hari_ini = explode('-', $tanggal);
        $rencana_pengerjaan = explode('-', $tanggal_mulai['tgl_mulai']);
        $rp = explode(' ', $rencana_pengerjaan[2]);
        if ($hari_ini[2] < $rp[0]) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Kegiatan belum bisa diselesaikan
          </div>');
            redirect('masterproyek/detail/' . $kegiatan['proyek_id']);
        }
        $data = [
            'status_kegiatan' => 2,
            'tgl_selesai' => date('Y-m-d H:i:s'),
        ];
        $this->db->where('id', $id);
        $this->db->update('t_kegiatan', $data);
        $data['kegiatan'] = $this->db->get_where('t_kegiatan', ['id' => $id])->row_array();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Kegiatan berhasil diselesaikan
          </div>');
        redirect('masterproyek/detail/' . $data['kegiatan']['proyek_id']);
    }
    public function updateTimeline()
    {
        $data = [
            'status' => 1,
            'id_proyek' => $this->input->post('id_proyek'),
        ];
        $this->Proyek_model->updateTimeline($data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Proyek berhasil ditambahkan
          </div>');
        redirect('masterproyek/timeline');
    }
    public function delete($id)
    {
        $this->Proyek_model->deleteData($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Proyek berhasil dihapus
          </div>');
        redirect('masterproyek');
    }
    public function hapuskegiatan($id)
    {
        $data['kegiatan'] = $this->db->get_where('t_kegiatan', ['id' => $id])->row_array();
        $this->Proyek_model->deleteKegiatan($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Kegiatan berhasil dihapus
          </div>');
        redirect('masterproyek/detail/' . $data['kegiatan']['proyek_id']);
    }


    public function detail($id)
    {
        $data['title'] = 'Master proyek';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $data['proyek'] = $this->db->get_where('t_proyek', ['id_proyek' => $id])->row_array();
        $data['kegiatan'] = $this->Proyek_model->kegiatanProyek($id);
        $data['persentase'] = $this->Proyek_model->persentaseKegiatan($id);
        $this->form_validation->set_rules('nama_kegiatan', 'Nama Kegiatan', 'required');
        $this->form_validation->set_rules('jenis_kegiatan', 'Jenis Kegiatan', 'required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
        $this->form_validation->set_rules('bobot', 'Bobot', 'required');
        $this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'required');
        $this->form_validation->set_rules('tgl_rencana_selesai', 'Tanggal Rencana Selesai', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('proyek/detailProyek', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'nama_kegiatan' => $this->input->post('nama_kegiatan'),
                'jenis_kegiatan' => $this->input->post('jenis_kegiatan'),
                'deskripsi' => $this->input->post('deskripsi'),
                'tgl_mulai' => $this->input->post('tgl_mulai'),
                'tgl_rencana_selesai' => $this->input->post('tgl_rencana_selesai'),
                'bobot' => $this->input->post('bobot'),
                'status_kegiatan' => 0,
                'proyek_id' => $id,
            ];

            
            if ($data['tgl_rencana_selesai'] < $data['tgl_mulai'] ) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Tanggal Rencana tidak boleh kurang dari tanggal mulai!
            </div>');
                redirect('masterproyek/detail/' . $id);
            }
            $persentase = $this->input->post('persentase');
            $bobot = $this->input->post('bobot');
            if (($bobot + $persentase) > 100) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Total Bobot melebihi 100%
            </div>');
                redirect('masterproyek/detail/' . $id);
            }
            $this->db->insert('t_kegiatan', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Kegiatan berhasil ditambahkan
            </div>');
            redirect('masterproyek/detail/' . $id);
        }
    }
    public function cancelProyek($id)
    {
        $this->Proyek_model->cancelData($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Proyek berhasil dicancel
          </div>');
        redirect('masterproyek/timeline');
    }

    public function selesaiProyek($id)
    {
        $query = "SELECT SUM(bobot) as bobot from t_kegiatan WHERE proyek_id = $id";
        $result = $this->db->query($query)->row_array();
        if ($result['bobot'] < 100) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
             Progress Belum 100%!
            </div>');
            redirect('masterproyek/detail/' . $id);
        }
        $data = [
            'status' => 2
        ];
        $this->db->where('id_proyek', $id);
        $this->db->update('t_proyek', $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Proyek berhasil diselesaikan
            </div>');
        redirect('masterproyek/timeline');
    }
    public function pdf()
    {
        // $this->load->library('dompdf_gen');
        // $data['proyek'] = $this->Proyek_model->getProyekpdf();
        // $this->load->view('proyek/laporan_pdf', $data);

        // $paper_size = 'A4';
        // $orientation = 'potrait';
        // $html = $this->output->get_output();
        // $this->dompdf->set_paper($paper_size,$orientation);
        // $this->dompdf->load_html($html);
        // $this->dompdf->render();
        // $this->dompdf->stream("laporan_" . date('Y-m-d').".pdf", array('Attachment' => 0));
        // panggil library yang kita buat sebelumnya yang bernama pdfgenerator
        $this->load->library('pdfgenerator');

        // title dari pdf
        $data['proyek'] = $this->Proyek_model->getProyekpdf();

        // filename dari pdf ketika didownload
        $file_pdf = 'Laporan Timeline ' . date('d-m-Y', strtotime(date('Y-m-d')));
        // setting paper
        $paper = 'A4';
        //orientasi paper potrait / landscape
        $orientation = "portrait";

        $html = $this->load->view('proyek/laporan_pdf', $data, true);

        // run dompdf
        $this->pdfgenerator->generate($html, $file_pdf, $paper, $orientation);
    }
}
