<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Masterclient extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        
    }
    public function index()
    {
        $data['title'] = 'Master Client';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        if ($data['user']['role_id'] == 2 || $data['user']['role_id'] == 1) {
            
            $data['client']= $this->User_model->dataClient();
            $this->form_validation->set_rules('nama_client', 'Nama Client', 'required');
            $this->form_validation->set_rules('no_telp', 'No Telepon', 'required');
            $this->form_validation->set_rules('username', 'username', 'required');
            $this->form_validation->set_rules('password', 'password', 'required');
    
            if ($this->form_validation->run() == false) {
                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('client/index', $data);
                $this->load->view('templates/footer');
            } else {
                $data = [
                    'nama' => $this->input->post('nama_client'),
                    'username' => $this->input->post('username'),
                    'password' => $this->input->post('password'),
                    'is_active' => 1,
                    'role_id' => 4,
                ];
    
                $user = $this->db->insert('user', $data);
                var_dump($user);
                die;
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Sub Menu berhasil ditambahkan
              </div>');
                redirect('menu/submenu');
            }
        } else {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('blank/404', $data);
            $this->load->view('templates/footer');
        }
    }
}
