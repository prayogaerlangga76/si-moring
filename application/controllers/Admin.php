<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    // public function __construct()
    // {
    //     parent::__construct();
    //     $this->load->model('Tagihan_model');
    //     $this->load->model('Pelanggan_model');
    // }
    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $data['client'] = $this->db->count_all_results('user');
        $this->db->where('status', 0);
        $data['proyekWL'] = $this->db->count_all_results('t_proyek');
        $this->db->where('status', 1);
        $data['proyekP'] = $this->db->count_all_results('t_proyek');
        $this->db->where('status', 2);
        $data['proyekF'] = $this->db->count_all_results('t_proyek');

        if ($data['user']['role_id'] == 2 || $data['user']['role_id'] == 1) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/index', $data);
            $this->load->view('templates/footer');
        } else {

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('blank/404', $data);
            $this->load->view('templates/footer');
        }
    }
}
