<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Usermanagement extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
    }
    public function index()
    {
        $data['title'] = 'User Management';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $data['user_management'] = $this->User_model->getDataUser();
        $data['role'] = $this->User_model->getDataRole();
        if ($data['user']['role_id'] == 2 || $data['user']['role_id'] == 1) {
            
            $this->form_validation->set_rules('nama', 'Nama', 'required');
            $this->form_validation->set_rules('no_hp', 'No Hp', 'required');
            $this->form_validation->set_rules('alamat', 'Alamat', 'required');
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('role_id', 'Role', 'required');
            if ($this->form_validation->run() == false) {
                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('manage/index', $data);
                $this->load->view('templates/footer');
            } else {
                $data = [
                    'nama' => $this->input->post('nama'),
                    'no_hp' => $this->input->post('no_hp'),
                    'alamat' => $this->input->post('alamat'),
                    'username' => $this->input->post('username'),
                    'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                    'role_id' => $this->input->post('role_id'),
                    'is_active' => 1,
                    'date_created' => time()
                ];
                $this->db->insert('user', $data);
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                User berhasil ditambahkan
              </div>');
                redirect('usermanagement');
            }
        } else {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('blank/404', $data);
            $this->load->view('templates/footer');
        }
        
    }

    public function update($id){
        $data = [
            'nama' => $this->input->post('nama'),
            'no_hp' => $this->input->post('no_hp'),
            'alamat' => $this->input->post('alamat'),
            'username' => $this->input->post('username'),
            'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            'role_id' => $this->input->post('role_id'),
        ];
        $this->User_model->updateData($id, $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            User berhasil diubah
          </div>');
        redirect('usermanagement');
    }

    public function delete($id)
    {
        $this->User_model->deleteData($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            User berhasil dihapus
          </div>');
        redirect('usermanagement');
    }
}
